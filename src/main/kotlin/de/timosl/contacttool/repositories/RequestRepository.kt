package de.timosl.contacttool.repositories

import de.timosl.contacttool.model.Request
import de.timosl.contacttool.model.VerificationState
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface RequestRepository: JpaRepository<Request, Long> {

    @Query
    fun findByState(state: VerificationState, pageRequest: Pageable): Page<Request>
}