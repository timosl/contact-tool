package de.timosl.contacttool.services

import de.timosl.contacttool.ContactToolConfig
import org.springframework.context.MessageSource
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import java.util.*

@Service
class MailService(
        private val mailSender: JavaMailSender,
        private val templateEngine: TemplateEngine,
        private val config: ContactToolConfig,
        private val messageSource: MessageSource
) {

    fun sendMail(message: String, name: String, mail: String, phone: String?, locale: Locale) {
        if(config.mailReceiverAddress.isEmpty() || config.mailSenderAddress.isEmpty()) {
            return
        }

        val ctx = Context().apply {
            setVariable("config", config)
            setVariable("message", message)
            setVariable("name", name)
            setVariable("mail", mail)
            setVariable("phone", phone)
        }

        val mimeMailMessage = this.mailSender.createMimeMessage().apply {
            MimeMessageHelper(this).apply {
                setSubject(messageSource.getMessage("mail.subject", null,  locale))
                setFrom(config.mailSenderAddress)
                setTo(config.mailReceiverAddress)
                setText(templateEngine.process("mail", ctx), true)
            }
        }

        mailSender.send(mimeMailMessage)
    }
}