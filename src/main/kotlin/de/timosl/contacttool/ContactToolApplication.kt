package de.timosl.contacttool

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.servlet.LocaleResolver
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver
import java.util.*

@SpringBootApplication
@EnableConfigurationProperties
@ConfigurationPropertiesScan
class ContactToolApplication {

	@Bean
	fun localeResolver(): LocaleResolver {
		val localResolver = AcceptHeaderLocaleResolver()
		localResolver.defaultLocale = Locale.US
		return localResolver
	}
}

fun main(args: Array<String>) {
	runApplication<ContactToolApplication>(*args)
}