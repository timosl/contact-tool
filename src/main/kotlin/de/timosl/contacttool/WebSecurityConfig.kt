package de.timosl.contacttool

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableWebSecurity
class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http.authorizeRequests {
            it.antMatchers("/").permitAll()
        }
        http.authorizeRequests {
            it.antMatchers("/admin/**").authenticated()
        }
        http.httpBasic {
            it.realmName("Adminstrator Area")
        }
        http.csrf {
            it.disable()
        }
    }
}