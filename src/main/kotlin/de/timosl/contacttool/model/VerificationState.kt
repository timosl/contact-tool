package de.timosl.contacttool.model

enum class VerificationState {
    PENDING,VALID,FILTERED,ERROR
}