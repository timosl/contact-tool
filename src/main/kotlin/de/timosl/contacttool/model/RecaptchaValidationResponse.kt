package de.timosl.contacttool.model

import com.fasterxml.jackson.annotation.JsonProperty

class RecaptchaValidationResponse {
    @JsonProperty("success")
    var success: Boolean = false

    @JsonProperty("hostname")
    var hostname: String? = null

    @JsonProperty("challenge_ts")
    var challengeTimestamp: String? = null

    @JsonProperty("error-codes")
    var errorCodes: List<String> = emptyList()
}