package de.timosl.contacttool.model

import java.time.Instant
import javax.persistence.*

@Entity
data class Request(
        val name: String,

        val mail: String,

        @Lob
        val message: String,

        @Lob
        @Column(nullable = true)
        val captchaString: String?,

        @Enumerated(EnumType.STRING)
        val state: VerificationState = VerificationState.PENDING,

        val received: Instant = Instant.now(),

        @Column(nullable = true)
        val phone: String? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "request_id_seq")
        @SequenceGenerator(name = "request_id_seq", sequenceName = "sq_request_id", allocationSize = 1, initialValue = 0)
        val id: Long = 0
)