package de.timosl.contacttool

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("contact-tool")
data class ContactToolConfig (
    /**
     * The name of the application. Customize this to your liking as it
     * will be displayed on the integrated form.
     */
    val appTitle: String = "Contact-Tool",
    /**
     * The secret key that was provided to you by Google for your reCaptcha
     * protected website.
     */
    val secretKey: String = "",
    /**
     * The public site key that was provided to you by Google for your
     * reCaptcha protected website.
     */
    val siteKey: String = "",
    /**
     * The URL your want your visitors to be redirected to after successfully
     * submitting a form.
     */
    val redirectTarget: String = "",
    /**
     * The sender address used in the notification e-mail sent by the system
     * when a user has successfully submitted a form.
     */
    val mailSenderAddress: String = "",
    /**
     * The e-mail address that will receive the notifications when a user has
     * successfully submitted a form.
     */
    val mailReceiverAddress: String = "",
    /**
     * The maximum amount of characters inside a message that will be accepted
     * by the system.
     */
    val maxMessageLength: Int = 4096
)