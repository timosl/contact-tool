package de.timosl.contacttool.controller.formatter

import org.ocpsoft.prettytime.PrettyTime
import org.springframework.format.Formatter
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

@Component
class InstantFormatter: Formatter<Instant> {

    override fun print(instant: Instant, locale: Locale): String {
        return PrettyTime(locale).format(Date.from(instant))
    }

    override fun parse(text: String, locale: Locale): Instant {
        return Instant.now()
    }
}