package de.timosl.contacttool.controller.formatter

import de.timosl.contacttool.model.VerificationState
import org.springframework.context.MessageSource
import org.springframework.format.Formatter
import org.springframework.stereotype.Component
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import java.util.*

@Component
class VerificationStateFormatter(
        private val messageSource: MessageSource
): Formatter<VerificationState> {

    override fun print(state: VerificationState, locale: Locale): String {
        return when(state) {
            VerificationState.FILTERED -> messageSource.getMessage("request.state.filtered", null, locale)
            VerificationState.VALID -> messageSource.getMessage("request.state.valid", null, locale)
            VerificationState.PENDING -> messageSource.getMessage("request.state.pending", null, locale)
            VerificationState.ERROR -> messageSource.getMessage("request.state.error", null, locale)
        }
    }

    override fun parse(text: String, locale: Locale): VerificationState {
        return when(text) {
            messageSource.getMessage("request.state.filtered", null, locale) -> VerificationState.FILTERED
            messageSource.getMessage("request.state.valid", null, locale) -> VerificationState.VALID
            messageSource.getMessage("request.state.pending", null, locale) -> VerificationState.PENDING
            messageSource.getMessage("request.state.error", null, locale) -> VerificationState.ERROR
            else -> throw IllegalArgumentException("Unknown state: $text")
        }
    }
}