package de.timosl.contacttool.controller

import java.lang.Integer.max
import java.lang.Integer.min

data class Pagination(
        val currentPage: Int,
        private val totalPages: Int
) {
    companion object {
        const val VISIBLE_PAGES = 5
    }

    fun getTotalPages(): Int {
        return max(0, totalPages - 1)
    }

    fun getVisiblePages(): IntRange {
        val minimum = max(0, currentPage - VISIBLE_PAGES)
        val maximum = min(totalPages - 1, currentPage + VISIBLE_PAGES)
        return IntRange(minimum, maximum)
    }

    fun isLastPage(): Boolean {
        return currentPage >= totalPages - 1
    }

    fun isFirstPage(): Boolean {
        return currentPage <= 0
    }
}