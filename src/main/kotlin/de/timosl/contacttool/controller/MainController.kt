package de.timosl.contacttool.controller

import com.fasterxml.jackson.databind.ObjectMapper
import de.timosl.contacttool.ContactToolConfig
import de.timosl.contacttool.model.RecaptchaValidationResponse
import de.timosl.contacttool.model.Request
import de.timosl.contacttool.model.VerificationState
import de.timosl.contacttool.repositories.RequestRepository
import de.timosl.contacttool.services.MailService
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request.Builder
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.transaction.annotation.Transactional
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import java.net.URI
import java.util.*

@Controller
@RequestMapping("/")
class MainController(
        private val requestRepository: RequestRepository,
        private val objectMapper: ObjectMapper,
        private val config: ContactToolConfig,
        private val mailService: MailService,
        private val messageSource: MessageSource
) {
    private val httpClient: OkHttpClient = OkHttpClient()

    @GetMapping
    fun getMain(model: Model): String {
        model.addAttribute("config", config)
        return "index"
    }

    @Transactional
    @PostMapping
    fun postMain(
            @RequestParam(required = true) name: String,
            @RequestParam(required = true) mail: String,
            @RequestParam(required = true) message: String,
            @RequestParam(required = false, value="g-recaptcha-response") captchaResponse: String?,
            @RequestParam(required = false) phone: String?,
            locale: Locale
    ): ResponseEntity<Any> {
        val newReq = Request(
                name,
                mail,
                message,
                phone = phone,
                captchaString = captchaResponse
        )
        requestRepository.save(newReq)

        val verificationRequest = Builder()
                .url("https://www.google.com/recaptcha/api/siteverify")
                .post(FormBody.Builder()
                        .add("secret", config.secretKey)
                        .add("response", captchaResponse.orEmpty())
                        .build())
                .build()

        httpClient.newCall(verificationRequest).execute().use { response ->
            val body = response.body()
            if(response.isSuccessful && body != null) {
                val responseObj = objectMapper.readValue(body.charStream(), RecaptchaValidationResponse::class.java)
                if(responseObj.success) {
                    requestRepository.save(newReq.copy(state = VerificationState.VALID))
                    mailService.sendMail(message, name, mail, phone, locale)

                    return ResponseEntity
                            .status(HttpStatus.SEE_OTHER)
                            .location(URI(config.redirectTarget))
                            .contentType(MediaType.TEXT_HTML)
                            .build()
                } else {
                    requestRepository.save(newReq.copy(state = VerificationState.FILTERED))
                    return ResponseEntity
                            .status(HttpStatus.FORBIDDEN)
                            .contentType(MediaType.TEXT_PLAIN)
                            .body(messageSource.getMessage("err.form.forbidden", null, locale))
                }
            } else {
                requestRepository.save(newReq.copy(state = VerificationState.ERROR))
                return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .contentType(MediaType.TEXT_PLAIN)
                        .body(messageSource.getMessage("err.form.failed", null, locale))
            }
        }
    }
}
