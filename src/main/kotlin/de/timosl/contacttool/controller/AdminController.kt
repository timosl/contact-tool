package de.timosl.contacttool.controller

import de.timosl.contacttool.repositories.RequestRepository
import de.timosl.contacttool.model.VerificationState
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Controller
import org.springframework.transaction.annotation.Transactional
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import java.util.*

@Controller
@RequestMapping("/admin")
class AdminController(
        private val requestRepository: RequestRepository
) {

    companion object {
        private const val DEFAULT_PAGE_SIZE = 25
    }

    private val sortDefault =  Sort.by(Sort.Direction.DESC, "received")

    @Transactional
    @GetMapping
    fun getAllList(@RequestParam page: Int?, model: Model, locale: Locale): String {
        val pageResult = requestRepository.findAll(PageRequest.of(page ?: 0,DEFAULT_PAGE_SIZE, sortDefault))
        val pagination = Pagination(pageResult.number, pageResult.totalPages)
        model.addAttribute("requests", pageResult)
        model.addAttribute("pagination", pagination)
        return "requests_all"
    }

    @Transactional
    @GetMapping("/filtered")
    fun getFilteredList(@RequestParam page: Int?, model: Model, locale: Locale): String {
        val pageResult = requestRepository.findByState(VerificationState.FILTERED, PageRequest.of(page ?: 0,DEFAULT_PAGE_SIZE, sortDefault))
        val pagination = Pagination(pageResult.number, pageResult.totalPages)
        model.addAttribute("requests", pageResult)
        model.addAttribute("pagination", pagination)
        model.addAttribute("requests", pageResult)
        return "requests_filtered"
    }

    @Transactional
    @GetMapping("/valid")
    fun getValidList(@RequestParam page: Int?, model: Model, locale: Locale): String {
        val pageResult = requestRepository.findByState(VerificationState.VALID, PageRequest.of(page ?: 0, DEFAULT_PAGE_SIZE, sortDefault))
        val pagination = Pagination(pageResult.number, pageResult.totalPages)
        model.addAttribute("requests", pageResult)
        model.addAttribute("pagination", pagination)
        model.addAttribute("requests", pageResult)
        return "requests_valid"
    }

    @Transactional
    @GetMapping("/id/{id}")
    fun getSingle(@PathVariable id: Long, model: Model, locale: Locale): String {
        requestRepository.findById(id).ifPresent {
            model.addAttribute("req", it)
        }
        return "requests_single"
    }
}