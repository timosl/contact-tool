# 1.2.3

- Fixes an error with E-Mail links on the request detail page

# 1.2.2

- Fixes an error with E-Mail links in notification mails

# 1.2.1

- Fixes an error with pagination on the admin page

# 1.2.0

- Display pagination elements on the admin page
- Fixes a problem with the healthcheck included in the Docker image

# 1.1.0

- Defined the license for the project
- Move from Hotspot JVM to OpenJ9 for the Docker container

# 1.0.0

The initial release
