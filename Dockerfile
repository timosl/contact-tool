FROM gradle:7.0.2-jdk16-hotspot AS build
WORKDIR /home/gradle/project

COPY src/ src/
COPY build.gradle.kts .
COPY settings.gradle.kts .
RUN gradle build --no-daemon

FROM adoptopenjdk/openjdk16-openj9:alpine AS release
MAINTAINER Timo Schlömer
WORKDIR /opt/app
RUN ["mkdir", "/opt/app/data"]
COPY --from=build /home/gradle/project/build/libs/contact-tool-*.jar /opt/app/contact-tool.jar
CMD ["java", "-jar", "/opt/app/contact-tool.jar", "--spring.profiles.active=docker"]
VOLUME /opt/app/data
EXPOSE 8080
