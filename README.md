# Contact-Tool

A web-application for allowing anonymous users to submit contact forms using reCaptcha to filter out spam messages.

# Getting started

You can either run the application directly from the executable JAR file, or use the Docker image to run it inside a container.

## Native

The tool is written in Kotlin and only requires an installed JRE compatible to Java 11  or higher.
You can start the tool in most graphical environments by simply double-clicking the JAR file or from the CLI with the command  `java -jar contact-tool.jar`.

Without any custom configuration the tool will start with an in-memory database and no e-mail reports.
The minimum configuration only requires your reCaptcha keys by setting the `contact-tool.site-key` and `contact-tool.secret-key` values in the config file.
You can create a configuration by creating a file called `application.properties` in the same folder as the executable.

###  Sample configuration

```properties
# The database credentials. Omit all of them to use the in-memory database
spring.datasource.url=jdbc:sqlite:example.db
spring.datasource.username=dbuser
spring.datasource.password=dbpassword

# The credentials for accessing the admin section of the web page
spring.security.user.name=admin
spring.security.user.password=admin

# The smtp credentials for sending e-mail reports. Omit all of them to disable reports
spring.mail.host=mailserver.com
spring.mail.username=mailuser
spring.mail.password=mailpassword

# The title that a user will see in the integrated web form
contact-tool.app-title=A Sample Title

# The reCaptcha site keys 
contact-tool.site-key=ABC123
contact-tool.secret-key=DEFG456

# The URL a user will be redirected to after successfully submitting a form
contact-tool.redirect-target=https://www.example.com

# The sender and receiver e-mail addresses to use for sending reports
contact-tool.mail-sender-address=contact@example.com
contact-tool.mail-receiver-address=admin@example.com
```

## Docker

You can use a pre-built Docker image from Docker-Hub or you can create an image yourself.
If you have Docker locally installed you can build the image with the following command:

```shell
docker build -t contact-tool:latest  .
```

You can configure the tool by setting the corresponding environment variables.
Those can either be defined manually or be defined inside an environment file.

### Sample environment file

```properties
DB_URL=jdbc:sqlite:example.db
DB_USERNAME=dbuser
DB_PASSWORD=dbpassword
USER_NAME=admin
USER_PASSWORD=admin
MAIL_HOST=mailserver.com
MAIL_USERNAME=mailuser
MAIL_PASSWORD=mailpassword
APP_TITLE=A Sample Title
APP_CAPTCHA_SITE_KEY=ABC123
APP_CAPTCHA_SECRET_KEY=DEFG456
APP_REDIRECT_TARGET=https://www.example.com
MAIL_SENDER=contact@example.com
MAIL_RECEIVER=admin@example.com
```

### Usage with docker-compose

You can use the Docker image of the tool together with other images (like a database for example) to create an entire stack using `docker-compose`.
Here is an example of a `docker-compose.yml` that creates the entire stack.

```yaml
version: "3"

services:
  contact-tool:
    image: timosl/contact-tool:latest
    volumes:
      - dbdata:/var/lib/postgresql/data
    env_file:
      - contact-tool.env
    ports:
      - 80:8080
    restart: on-failure
    depends_on:
      - postgres

  postgres:
    image: postgres:12-alpine
    ports:
      - 5432:5432
    restart: on-failure

volumes:
  dbdata:
```

The tool will then be available under port 80 of the machine and use a postgres database for storing all requests.
The environment file must be configured accordingly.

# Building yourself

The project uses Gradle for compiling the code.
The Gradle Wrapper executable inside the repository can be used if no version of Gradle is locally installed.

You can compile the code using the Gradle Wrapper like this:

```shell
./gradlew clean build
```

You can create an executable file like this:

```shell
./gradlew clean bootJar
```

The executable will be created in `build/lib/`.